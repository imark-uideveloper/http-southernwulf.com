var h = jQuery("#header-sroll");
jQuery(window).scroll(function() {
    var windowpos = jQuery(window).scrollTop();
    if (windowpos >= 200) {
        h.addClass("small");
    } else {
        h.removeClass("small");
    }
});

/* Page-Scroll-Bar */
jQuery(document).on('click', '.menu-item', function(e) {
    e.preventDefault();
    var index = jQuery(this).attr('href');
    if (index == 'undefined' || index == undefined) {
        jQuery('li').removeClass('current-menu-item');
        jQuery(this).addClass('current-menu-item');
        var index = jQuery(this).find('a').text();
        var headerClass = jQuery('#header-sroll').hasClass('small');
        index = (index == "Home" ? 'home' : index);
        index = (index == "Our Company" ? 'company' : index);
        index = (index == "Product Categories" ? 'product_section' : index);
        index = (index == "Focus Areas" ? 'focus_section' : index);
        index = (index == "Mission" ? 'mission' : index);
        index = (index == "Founders" ? 'founder' : index);
        index = (index == "Contact Us" ? 'contact-us' : index);
    }

    if (jQuery('#' + index).length > 0) {
        jQuery('html,body').animate({
            scrollTop: jQuery('#' + index).offset().top - 73
        }, 2500);

    }
});
jQuery(window).on("scroll", onScroll);

function onScroll(event) {
    var scrollPos = jQuery(document).scrollTop();
    jQuery('#menu-primary-menu li a').each(function() {
        var currLink = jQuery(this);
        var refElement = jQuery(currLink.attr("href"));
        if (refElement.position().top - 73 <= scrollPos && refElement.position().top + refElement.height() - 73 > scrollPos) {
            jQuery('#menu-primary-menu li').removeClass("current-menu-item");

            currLink.parents('li').addClass("current-menu-item");
        }

        if (scrollPos > 4235) {
            jQuery('#menu-primary-menu li').removeClass("current-menu-item");
            jQuery('#menu-primary-menu li').last().addClass("current-menu-item");
        }

    });
}

/* //Page-Scroll-Bar */

/* Accordion-Slider-JS-Here  */
$(document).ready(function() {
    var $item = 0,
        $itemNo = $(".hero figure").length;

    function transitionSlide() {
        $item++;
        if ($item > $itemNo - 1) {
            $item = 0;
        }
        $(".hero figure").removeClass("on");
        $(".hero figure")
            .eq($item)
            .addClass("on");
    }
    var $autoTransition = setInterval(transitionSlide, 555500);

    $(".hero figure").click(function() {
        clearInterval($autoTransition);
        $item = $(this).index();
        $(".hero figure").removeClass("on");
        $(".hero figure")
            .eq($item)
            .addClass("on");
        $autoTransition = setInterval(transitionSlide, 555500);
    });
});

jQuery(function($) {
    jQuery(window).on("load", function() {

        jQuery("#demo").mCustomScrollbar({
            keyboard: {
                enable: true,
                scrollType: "stepless",
                scrollAmount: "auto"
            }
        });

    });
})(jQuery);

/* Page-Scroll-bottom-to-Top */
$(window).scroll(function() {
    if ($(this).scrollTop() > 50) {
        $('.scrolltop:hidden').stop(true, true).fadeIn();
    } else {
        $('.scrolltop').stop(true, true).fadeOut();
    }
});
$(function() { $(".scroll").click(function() { $("html,body").animate({ scrollTop: $("body").offset().top }, "1000"); return false }) })